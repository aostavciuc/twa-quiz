import {combineReducers} from "redux";
import QuizReducer from "./QuizReducer";
import AnswerReducer from "./AnswerReducer";

const rootReducer = combineReducers({
    quiz: QuizReducer,
    answer: AnswerReducer
});

export default rootReducer;
