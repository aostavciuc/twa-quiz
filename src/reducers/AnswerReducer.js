import {ANSWER, FETCH_QUIZ} from "../actions";

const INITIAL_STATE = {answers: []};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_QUIZ: {
            return INITIAL_STATE;
        }
        case ANSWER: {
            const {index, answer} = action.payload;
            state.answers[index] = answer;
            return {answers: state.answers};
        }
        default: {
            return state;
        }
    }
}
