import axios from "axios";

export const FETCH_QUIZ = "FETCH_QUIZ";
export const ANSWER = "ANSWER";
const ROOT_URL = "https://opentdb.com";

export function fetch() {
    const request = axios.get(`${ROOT_URL}/api.php?amount=10`);
    return {
        type: FETCH_QUIZ,
        payload: request
    };
}

export function answer(index, answer) {
    return {
        type: ANSWER,
        payload: {index, answer}
    };
}

