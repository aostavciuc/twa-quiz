import React, {Component} from "react";
import AnswerComponent from "./AnswerComponent";
import _ from "lodash"

class Question extends Component {
    render() {
        const {index, item} = this.props;
        return (
            <div className="row">
                <div>{index + 1}: {_.unescape(item.question)}</div>
                <AnswerComponent correctAnswer={item.correct_answer}
                                 questionIndex={index}
                                 incorectAnswer={item.incorrect_answers}/>
            </div>
        );
    }
}

export default Question;
