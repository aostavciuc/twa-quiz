import React, {Component} from "react";
import {connect} from "react-redux";
import {fetch} from "../actions";
import Question from "./Question";

class QuizView extends Component {
    componentDidMount() {
        this.props.fetch();
    }

    checkQuiz = () => {
        const {quiz, answers} = this.props;
        let counter = 0;
        quiz.results.forEach(item => {
            if (answers.indexOf(item.correct_answer) > -1) {
                counter++;
            }
        });
        alert('Correct answers: ' + counter + ' / ' + quiz.results.length);
    };

    render() {
        const {quiz} = this.props;

        if (!quiz.results) {
            return <div>Loading...</div>;
        }

        return (
            <div>
                {quiz.results.map((item, index) => {
                    return <Question index={index} item={item}/>
                })}
                <button onClick={this.checkQuiz}> Check!</button>
            </div>
        );
    }
}

function mapStateToProps({quiz, answer}) {
    return {quiz: quiz, answers: answer.answers};
}

export default connect(mapStateToProps, {fetch})(QuizView);
