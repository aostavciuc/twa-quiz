import React, {Component} from "react";
import {connect} from "react-redux";
import {answer} from "../actions";
import _ from "lodash"

class AnswerComponent extends Component {

    state = {
        selected: null
    };

    onSelectAnswer = (index, answer) => {
        this.setState({selected: answer});
        this.props.answer(this.props.questionIndex, answer);
    };

    renderOption = (item, questionIndex) => {
        return <div className="answer-box" onClick={() => this.onSelectAnswer(questionIndex, item)}>
            <input type="radio" checked={item === this.state.selected}/>
            <span>{_.unescape(item)}</span>
        </div>
    };

    render() {
        const {correctAnswer, incorectAnswer} = this.props;
        let items = [...incorectAnswer, correctAnswer];
        _.shuffle(items);
        return <div>{items.map((item) => this.renderOption(item))}</div>;
    }
}

export default connect(null, {answer})(AnswerComponent);
